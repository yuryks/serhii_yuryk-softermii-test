# Test assignment #

This is short description of test assignment for iOS Swift developer position at http://www.softermii.com.

### General requirements: ###

* Project language — Swift
* 3rd part lib:
  - networking: Alamofire
  - image loading: SDWebImage/Kingfisher/Other
  - JSON parsing: SwiftyJSON/ObjectMapper/Other        
* Architectural patterns: MVC/MVVM/VIPER/
* Design patterns: Singleton, Facade, Delegate,
* API: instagram official API
* Language features: UIStackView, UICollectionView, AutoLayout, functional language features (map, filter…)
* Dependency manager: cocoapods
* Version Control: git
* Well organized code structure

### Short description  ###
Создать приложение для работы с Instagram API

* 1й скрин
  - логин, обязательно сделать валидацию полей ввода, обработку серверных ошибок
* 2й скрин
  - UIView(сделать как хедер) - информация о пользователе (кол-во подписчиков/подписок, картинка пользователя, основная информация информация).
 UICollectionView - отображает массив загруженных картинок пользователя: отобразить в 3 столбца
* 3й скрин
  - по нажатию на картинку открывается отдельный экран с детальным её изображением - здесь сделать возможность зума картинки и шаринга.
* UI - cделать максимально похожим на оригинальный апп.
* Создать репозиторий на гитхабе ‘your_name-softermii-test’
* Добавить .gitignore.
* Создать ветку ‘dev’, и все тестовое делать на ней.

### Необязательные условия ###
