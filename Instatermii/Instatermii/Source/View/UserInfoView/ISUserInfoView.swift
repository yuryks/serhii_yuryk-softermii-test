//
//  ISUserInfoView.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit

class ISUserInfoView: UIView, Reusable {

    //MARK: - Outlets
    @IBOutlet fileprivate var contentView: UIView?
    @IBOutlet fileprivate weak var postCountLabel: UILabel?
    @IBOutlet fileprivate weak var followedByCountLabel: UILabel?
    @IBOutlet fileprivate weak var followsCountLabel: UILabel?
    
    //MARK: - Properties
    
    //MARK: - View life
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadFromNib()
    }

}

//MARK: - Public methods
extension ISUserInfoView {
    func configureWith(posts postCount: String?, followers followedByCount: String?, andFollows followCount: String?) {
        UIView.animate(withDuration: 0.3) { 
            self.postCountLabel?.text = postCount
            self.followedByCountLabel?.text = followedByCount
            self.followsCountLabel?.text = followCount
        } 
    }
}

//MARK: - Private methods
fileprivate extension ISUserInfoView {
    func loadFromNib() {
        Bundle.main.loadNibNamed(ISUserInfoView.reuseIdentifier, owner: self, options: nil)
        guard let content = contentView else {
            return
        }
        
        addSubview(content)
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

    }
}
