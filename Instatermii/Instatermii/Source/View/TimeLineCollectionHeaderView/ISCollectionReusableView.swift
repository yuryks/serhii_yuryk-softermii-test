//
//  ISCollectionReusableView.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit

class ISCollectionReusableView: UICollectionReusableView, Reusable {
    //MARK: - Outlets
    
    @IBOutlet fileprivate weak var userInfoView: ISUserInfoView?
    @IBOutlet fileprivate weak var userAvatarImageView: UIImageView?
    @IBOutlet fileprivate weak var userNameLabel: UILabel?
    @IBOutlet fileprivate weak var imageLoadingActivityIndicator: UIActivityIndicatorView?
    
    //MARK: - Prorerties
    
    
    //MARK: - View life
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }   
}

//MARK: - Public methods
extension ISCollectionReusableView {
    
    /// Load user data from local storage to header view. NOTE! At this moment user must be loged in.
    func updateContent() {
        
        let user = InstagramUser.getUserFromCache()
        
        guard let url = URL(string: user?.avatarImageLink ?? "") else {
            return
        }
        
        self.userInfoView?.configureWith(posts: user?.media, followers: user?.followedBy, andFollows: user?.follows)
        self.userNameLabel?.text = user?.username
        self.userAvatarImageView?.kf.setImage(with: url)
    }
}

//MARK: - Private methods
fileprivate extension ISCollectionReusableView {
    func setUp() {
        userAvatarImageView?.kf.indicatorType = .activity
        
        userAvatarImageView?.layer.masksToBounds = true
        userAvatarImageView?.layer.borderWidth = 1
        userAvatarImageView?.layer.borderColor = UIColor.gray.cgColor
        userAvatarImageView?.layer.cornerRadius = 15
    }
}


