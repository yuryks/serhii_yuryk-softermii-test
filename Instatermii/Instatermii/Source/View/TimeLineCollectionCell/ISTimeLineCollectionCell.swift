//
//  ISTimeLineCollectionCell.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit

class ISTimeLineCollectionCell: UICollectionViewCell, Reusable {
//MARK: - Outlets
    @IBOutlet fileprivate weak var pictureImageView: UIImageView?

//MARK: - Properties
    
//MARK: - Cell life
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension ISTimeLineCollectionCell {
    /// Loads image to imageView
    ///
    /// - Parameter url: image link
    public func loadImage(from url:String?) {
        guard let url = URL(string: url ?? "") else {
            return
        }
        
        pictureImageView?.kf.indicatorType = .activity
        pictureImageView?.kf.setImage(with: url)
    }
}
