//
//  UIViewController+Extensions.swift
//  Instatermii
//
//  Created by Serhii on 8/8/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import Foundation

extension Reusable where Self: UIViewController {
    ///string identifier for view
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
