//
//  UIView+Extensions.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import Foundation

extension Reusable where Self: UIView {
    ///string identifier for view
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
