//
//  ISPostDetailsVC.swift
//  Instatermii
//
//  Created by Serhii on 8/8/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit
import Kingfisher

class ISPostDetailsVC: UIViewController, Reusable {

//MARK: - Outlets
    @IBOutlet fileprivate weak var scrollView: UIScrollView?
    @IBOutlet fileprivate weak var postImageView: UIImageView?
    
//MARK: - Properties
    public var imageLink: String? {
        didSet {
            self.loadImage()
        }
    }
//MARK: - VC Life
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationItem()
        self.setupScrollView()
        self.loadImage()
    }

}
//MARK: - Public methods
//MARK: - UIScrollViewDelegate
extension ISPostDetailsVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return postImageView
    }
}

//MARK: - Private methods
fileprivate extension ISPostDetailsVC {
    
    /// Loads image into imageView on scrollView
    func loadImage() {
        guard let url = URL(string: imageLink ?? "") else {
            debugPrint("url failed \(#line)")
            return
        }
        
        postImageView?.kf.indicatorType = .activity
        postImageView?.kf.setImage(with: url)
    }
    
    /// Scroll view basic preparation
    func setupScrollView() {
        scrollView?.delegate = self
        scrollView?.minimumZoomScale = 1.0
        scrollView?.maximumZoomScale = 6.0
    }
    
    /// Navigation basic preparation
    func setUpNavigationItem() {
        title = "Post details"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(shareTapped))
    }
    
    @objc func shareTapped() {
        let alert = UIAlertController(title: "Send to cosmos?", message: "This image will be sent to black hole", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
}
