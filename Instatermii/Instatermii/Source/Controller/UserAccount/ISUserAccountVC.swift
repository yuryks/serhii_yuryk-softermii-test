//
//  ISUserAccountVC.swift
//  Instatermii
//
//  Created by Serhii on 8/4/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit
import Kingfisher

class ISUserAccountVC: UIViewController {
    
//MARK: - Outlets
    @IBOutlet fileprivate weak var userAvatarImageView: UIImageView?
    @IBOutlet fileprivate weak var userNameLabel: UILabel?
    @IBOutlet fileprivate weak var editProfileButton: UIButton?
    @IBOutlet fileprivate weak var userInfoView: ISUserInfoView?
    @IBOutlet fileprivate weak var collectionView: UICollectionView?
    @IBOutlet fileprivate var configurationViewButtons: [UIButton]?
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView?
    
    //TODO: Create custom view. Not needed for test asignment
    @IBOutlet fileprivate weak var collectionConfigurationView: UIView?

//MARK: - Properties
    fileprivate var itemsPerRow: CGFloat = 3 {
        didSet {
            collectionView?.reloadData()
        }
    }
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 20.0, bottom: 0, right: 20.0)
    fileprivate let userAvatarImageViewCornerRadius: CGFloat = 15
    fileprivate let editProfileButtonCornerRadius: CGFloat = 5
    fileprivate let layerBorder: CGFloat = 1
    
    fileprivate var user: InstagramUser? {
        didSet {
            self.updateUI()
            self.loadUserImages() //TODO: Code duplicates
        }
    }
    
    /// Array of dictionaries with links to images (thumbnail, low_resolution, standard_resolution)
    fileprivate var images: [JSONDictionary]? {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    fileprivate var userAvatar: UIImage? {
        didSet {
            self.setUserAvatar()
        }
    }
    
//MARK: - VC Life
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpImageView()
        self.setUpEditProfileButton()
        self.setUpConfigurationViewButtons()
        self.setUpCollectionView()
        self.authenticate()
    }
}

//MARK: - UICollectionViewDataSource
extension ISUserAccountVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISTimeLineCollectionCell.reuseIdentifier, for: indexPath) as? ISTimeLineCollectionCell else {
            return UICollectionViewCell()
        }
        
        let imageDict = images?[indexPath.row]
        
        guard
            let thumbnail = imageDict?["thumbnail"] as? JSONDictionary,
            let url = thumbnail["url"] as? String
            
            else {
                fatalError("FOULD NOT LOAD URL FOR THUMBNAIL ")
        }
        
        cell.loadImage(from: url)
        
        return cell
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension ISUserAccountVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
}


//MARK: - Private methods
fileprivate extension ISUserAccountVC {
    
    /// Tries to autenticate user with OAuth. If error occures - shows allert.
    func authenticate() {
        
        do {
            try InstagramUser.authenticateWithOAuth { (instagramUserData) in
                self.user = instagramUserData
            }
        } catch let error {
            ISErrorManager.showAlert(with: error)
        }
    }
    
    func loadUser() {
        user = InstagramUser.getUserFromCache()
    }
    
    /// Update VC UI due to user data.
    func updateUI() {
        self.userInfoView?.configureWith(posts: user?.media,
                                         followers: user?.followedBy,
                                         andFollows: user?.follows)
        
        self.userNameLabel?.text = user?.username
        
        do {
            try self.loadUserAvatar(from: user?.avatarImageLink)
        } catch let error {
            ISErrorManager.showAlert(with: error)
        }
    }
    
    /// Basic preparation of userAvatarImageView
    func setUpImageView() {
        self.userAvatarImageView?.layer.masksToBounds = true
        self.userAvatarImageView?.layer.cornerRadius = userAvatarImageViewCornerRadius
        self.userAvatarImageView?.layer.borderWidth = layerBorder
        self.userAvatarImageView?.layer.borderColor = UIColor.gray.cgColor
    }
    
    /// Basic preparation of editProfileButton
    func setUpEditProfileButton() {
        self.editProfileButton?.layer.masksToBounds = true
        self.editProfileButton?.layer.cornerRadius = editProfileButtonCornerRadius
        self.editProfileButton?.layer.borderWidth = layerBorder
        self.editProfileButton?.layer.borderColor = UIColor.gray.cgColor
        self.editProfileButton?.addTarget(self, action: #selector(editProfileAction), for: .touchUpInside)
    }
    
    /// Basic preparation of configurationViewButtons
    func setUpConfigurationViewButtons() {
        self.configurationViewButtons?.forEach { (button) in
            switch button.tag {
            case 0:
                button.addTarget(self, action: #selector(gridButtonPressed), for: .touchUpInside)
            case 1:
                button.addTarget(self, action: #selector(listButtonPressed), for: .touchUpInside)
            default:
                button.addTarget(self, action: #selector(editProfileAction), for: .touchUpInside)
            }
            
        }
    }
    
    /// Collection view basic preparation
    func setUpCollectionView() {
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        collectionView?.register(UINib(nibName: ISTimeLineCollectionCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: ISTimeLineCollectionCell.reuseIdentifier)
        
    }
    
    /// Sets user avatar image in imageView if image loaded with Alamofire
    func setUserAvatar() {
        self.userAvatarImageView?.image = userAvatar
    }
    
    func loadUserAvatar(from link: String?) throws {
        guard let url = URL(string: link ?? "") else {
            throw ISErrorManager.ISError.errorLoadingAvatar
        }
        
        userAvatarImageView?.kf.indicatorType = .activity
        userAvatarImageView?.kf.setImage(with: url)
    }
    
    /// Loads all recent posts
    func loadUserImages() {
        activityIndicator?.startAnimating()
        InstagramUser.getUserImageList({imageDict in
            self.activityIndicator?.stopAnimating()
            self.images = imageDict
        })
    }
    
    /// Selector for all buttons on User profile screen
    @objc func editProfileAction() {
        let alert = UIAlertController(title: "Attention!", message: "No need to implement for test asignment", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func listButtonPressed() {
        itemsPerRow = 1
    }
    
    @objc func gridButtonPressed() {
        itemsPerRow = 3
    }
}
