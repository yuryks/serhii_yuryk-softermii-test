//
//  ISTabBarVC.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit

class ISTabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1 // TODO: Hardcoded value. Used to show user login first.
    }
}
