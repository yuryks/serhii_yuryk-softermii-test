//
//  ISUserTimeLineVC.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit

class ISUserTimeLineVC: UIViewController {

//MARK: - Outlets
    @IBOutlet fileprivate weak var collectionView: UICollectionView?
    @IBOutlet  weak var activityIndicator: UIActivityIndicatorView?
    
//MARK: - Properties
    fileprivate let itemsPerRow: CGFloat = 3
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 20.0, bottom: 0, right: 20.0)
    fileprivate let headerHeight: CGFloat = 112 // image height + bottom space + separator space  = 100 + 8 + 4
    
    /// Array of dictionaries with links to images (thumbnail, low_resolution, standard_resolution)
    fileprivate var images: [JSONDictionary]? {
        didSet {
            collectionView?.reloadData()
        }
    }
    
//MARK: - VC Life
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "User posts"
        self.prepareCollectionView()
        self.loadImages() //TODO: Code duplicates
    }
}

//MARK: - Public methods
extension ISUserTimeLineVC {
    
}

//MARK: - UICollectionViewDelegate
extension ISUserTimeLineVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = ISPostDetailsVC(nibName: ISPostDetailsVC.reuseIdentifier, bundle: nil)
        
        let imageDict = images?[indexPath.row]
        
        guard
            let thumbnail = imageDict?["standard_resolution"] as? JSONDictionary,
            let url = thumbnail["url"] as? String
            
            else {
                fatalError("COULD NOT LOAD URL FOR STANDARD_RESOLUTION ")
        }
        
        vc.imageLink = url
        
        self.show(vc, sender: self)
        
    }
}

//MARK: - UICollectionViewDataSource
extension ISUserTimeLineVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISTimeLineCollectionCell.reuseIdentifier, for: indexPath) as? ISTimeLineCollectionCell else {
            return UICollectionViewCell()
        }
        
        let imageDict = images?[indexPath.row]
        
        guard
            let thumbnail = imageDict?["thumbnail"] as? JSONDictionary,
            let url = thumbnail["url"] as? String
            
            else {
                fatalError("COULD NOT LOAD URL FOR THUMBNAIL ")
        }
        
        cell.loadImage(from: url)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            
            guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ISCollectionReusableView.reuseIdentifier, for: indexPath) as? ISCollectionReusableView else {
                return UICollectionReusableView()
            }
            
            header.updateContent()
            
            return header
            
        default:
            fatalError("UNEXPECTED ELEMENT KIND")
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension ISUserTimeLineVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {

        let paddingSpace = sectionInsets.left
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: headerHeight)
    }
}


//MARK: - Private methods
fileprivate extension ISUserTimeLineVC {
    
    /// Collection view basic preparation
    func prepareCollectionView() {
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        collectionView?.register(UINib(nibName: ISTimeLineCollectionCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: ISTimeLineCollectionCell.reuseIdentifier)
        collectionView?.register(UINib(nibName: ISCollectionReusableView.reuseIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ISCollectionReusableView.reuseIdentifier)

    }
    
    /// Loads all recent posts
    func loadImages() {
        activityIndicator?.startAnimating()
        InstagramUser.getUserImageList({imageDict in
            self.activityIndicator?.stopAnimating()
            self.images = imageDict
        })
    }
}
