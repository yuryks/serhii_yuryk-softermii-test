//
//  InstagramUserObject.swift
//  Instatermii
//
//  Created by Serhii on 8/4/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String:Any]

class InstagramUser: NSObject, NSCoding {
    
    //MARK: - Properties
    public var token: String?
    public var uid: String?
    public var bio: String?
    public var followedBy: String?
    public var follows: String?
    public var media: String?
    public var username: String?
    public var avatarImageLink: String?
    
    override var description: String {
        return "token = \(String(describing: self.token)),\n uid = \(String(describing: self.uid)),\n bio = \(String(describing: self.bio)),\n followedBy = \(String(describing: self.followedBy)),\n follows = \(String(describing: self.follows)),\n media = \(String(describing: self.media)),\n username = \(String(describing: self.username)),\n avatarImageLink = \(String(describing: self.avatarImageLink))"
    }
    
    //MARK: - Initializers
    /// Initializer with default values (empty strings).
    init(token: String? = "",
         uid: String? = "",
         bio: String? = "",
         followedBy: String? = "",
         follows: String? = "",
         media: String? = "",
         username: String? = "",
         avatarImageLink: String? = "") {
        
        self.token = token
        self.uid = uid
        self.bio = bio
        self.followedBy = followedBy
        self.follows = follows
        self.media = media
        self.username = username
        self.avatarImageLink = avatarImageLink
    }
    
    // MARK: - NSCoding protocol conformance.
    //In this case decided to use NSCoding - to save InstagramUser to UserDefaults.
    
    public required convenience init?(coder aDecoder: NSCoder) {
        //TODO: Not sure about guard , maybe exists more elegant way
        guard
            
            let token = aDecoder.decodeObject(forKey: InstagramUserField.token.rawValue) as? String,
            let uid = aDecoder.decodeObject(forKey: InstagramUserField.uid.rawValue) as? String,
            let bio = aDecoder.decodeObject(forKey: InstagramUserField.bio.rawValue) as? String,
            let followedBy = aDecoder.decodeObject(forKey: InstagramUserField.followedBy.rawValue) as? String,
            let follows = aDecoder.decodeObject(forKey: InstagramUserField.follows.rawValue) as? String,
            let media = aDecoder.decodeObject(forKey: InstagramUserField.media.rawValue) as? String,
            let username = aDecoder.decodeObject(forKey: InstagramUserField.username.rawValue) as? String,
            let avatarImageLink = aDecoder.decodeObject(forKey: InstagramUserField.avatarImageLink.rawValue) as? String
            
            else {
                fatalError("FATAL ERROR: Could not get instagram user from local storage") //TODO: JUST for information purposes
        }
        
        self.init(token: token, uid: uid, bio: bio, followedBy: followedBy, follows: follows, media: media, username: username, avatarImageLink: avatarImageLink)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.token, forKey: InstagramUserField.token.rawValue)
        aCoder.encode(self.uid, forKey: InstagramUserField.uid.rawValue)
        aCoder.encode(self.bio, forKey: InstagramUserField.bio.rawValue)
        aCoder.encode(self.followedBy, forKey: InstagramUserField.followedBy.rawValue)
        aCoder.encode(self.follows, forKey: InstagramUserField.follows.rawValue)
        aCoder.encode(self.media, forKey: InstagramUserField.media.rawValue)
        aCoder.encode(self.username, forKey: InstagramUserField.username.rawValue)
        aCoder.encode(self.avatarImageLink, forKey: InstagramUserField.avatarImageLink.rawValue)
    }
    
    func saveToLocalStorage() {
        let defaults = UserDefaults.standard
        let userObject = NSKeyedArchiver.archivedData(withRootObject: self)
        defaults.set(userObject, forKey: "userObject")
        defaults.synchronize()
    }
    
}

// MARK: - Public static methods
extension InstagramUser {
    
    /** Please NOTE. Third party lib used to get user profile token from redirect URI, because I use instagram OAuth registration without backend. In case backend exists, i will use it`s URL as redirect URI, and server will redirect me to link instatermii://oauth/token
     
     https://www.instagram.com/developer/clients/YOUR_APP_ID_HERE/edit/
     try to add custom (instatermii://) URI to you app at Security tab -> Valid redirect URIs.
     
     */
    public static func authenticateWithOAuth(_ completion: @escaping (_ user: InstagramUser?) -> ()) throws {
        
        let params = ["client_id": ISCredetialsInstagram.instagramClientId, SimpleAuthRedirectURIKey: ISCredetialsInstagram.instagramRedirectURI]
        
        SimpleAuth.configuration()["instagram"] = params
        SimpleAuth.authorize("instagram", options: [:]) { (result: Any?, error: Error?) in
            
            guard let result = result as? JSONDictionary else {
                return
                //throw ISErrorManager.ISError.errorConvertingResponse // i`d like to thow error here, but third party library did not expect it here. Decided not to edit third party lib for now.
            }
            
            do {
                let user = try self.createUserFrom(json: result)
                user?.saveToLocalStorage()
                completion(self.getUserFromCache())
            } catch let error {
                completion(nil)
                ISErrorManager.showAlert(with: error)
            }
            
            if error != nil {
                completion(nil)
                ISErrorManager.showAlert(with: error!) //force unwrap - checked for nil in if statement, but better to avoid
            }
        }
    }
    
    /// Loads InstagramUser object from UserDefaults
    ///
    /// - Returns: InstagramUser if success or nil if error
    public static func getUserFromCache() -> InstagramUser? {
        
        guard let userObject = UserDefaults.standard.data(forKey: "userObject") else {
            return nil
        }
        
        guard let instagramUser = NSKeyedUnarchiver.unarchiveObject(with: userObject) as? InstagramUser else{
            return nil
        }
        
        return instagramUser
    }
    
    /// Loads JSON from instagram server with information about all posts
    public static func getUserImageList(_ completion: @escaping (_ images: [JSONDictionary]?)->()) {
        guard let userToken = InstagramUser.getUserFromCache()?.token else {
            return
        }
        
        guard let url = URL(string: ISCredetialsInstagram.recentPostsURL + userToken) else {
            return
        }
        
        print("\n")
        debugPrint("url = \(url)")
        print("\n")
        
        var imagesDict = [JSONDictionary]()
        
        Alamofire.request(url).responseJSON { (responseData) in
            
            switch responseData.result {
            case .success(let result):
                
                guard let responseDict = result as? JSONDictionary else {
                    debugPrint("responseDict failed")
                    return
                }
                
                guard let data = responseDict["data"] as? [JSONDictionary] else {
                    debugPrint("data failed")
                    return
                }
                
                for dictionary in data {
                    
                    guard let images = dictionary["images"] as? JSONDictionary else {
                        debugPrint("images failed")
                        return
                    }
                    
                    imagesDict.append(images)
                }
                
                completion(imagesDict)
                
            case .failure(let error):
                completion(nil)
                ISErrorManager.showAlert(with: error)
            }
        }
    }
}
//MARK: - Private methods
fileprivate extension InstagramUser {
    
    /// Enumeration with InstagramUser property names. Created to avoid raw strings in code.
    enum InstagramUserField: String {
        case token,
        uid,
        bio,
        followedBy,
        follows,
        media,
        username,
        avatarImageLink
    }
    
    @discardableResult
    static func createUserFrom(json dictionary: JSONDictionary) throws -> InstagramUser? { //TODO: too many guards ???
        
        var user: InstagramUser?
        
        guard let token = (dictionary["credentials"] as? JSONDictionary)?["token"] as? String else {
            debugPrint("token FAILED")
            return nil
        }
        
        guard let uid = dictionary["uid"] as? String else {
            debugPrint("uid FAILED")
            return nil
        }
        
        guard
            let extra = dictionary["extra"] as? JSONDictionary,
            let rawInfo = extra ["raw_info"] as? JSONDictionary,
            let data = rawInfo["data"] as? JSONDictionary
            else {
                debugPrint("data FAILED")
                return nil
        }

        guard let counts = data["counts"] as? JSONDictionary else {
            debugPrint("data FAILED")
            return nil
        }
        
        let followed_by = String(describing: counts["followed_by"] ?? "")
        let follows = String(describing: counts["follows"] ?? "")
        let media = String(describing: counts["media"] ?? "")

        guard let bio = data["bio"] as? String else {
            debugPrint("bio FAILED")
            return nil
        }
        
        guard
            let userInfo = dictionary["user_info"] as? JSONDictionary,
            let username = userInfo["username"] as? String,
            let image = userInfo["image"] as? String
            else {
                debugPrint("username, image FAILED")
                return nil
        }

        user = InstagramUser(token: token, uid: uid, bio: bio, followedBy: followed_by, follows: follows, media: media, username: username, avatarImageLink: image)
        
        return user
    }
}
