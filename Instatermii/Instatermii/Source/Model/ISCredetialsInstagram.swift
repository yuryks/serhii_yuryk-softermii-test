//
//  ISCredetialsInstagram.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import Foundation

/// Instagram credentials storage 
struct ISCredetialsInstagram {
    static let instagramClientId = "98d45d79afe24cee87e2c1b6abe5f273"
    static let instagramRedirectURI = "http://softermii.com/"
    
    static let recentPostsURL = "https://api.instagram.com/v1/users/self/media/recent?access_token="
}
