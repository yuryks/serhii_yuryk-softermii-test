//
//  ISErrorManager.swift
//  Instatermii
//
//  Created by Serhii on 8/7/17.
//  Copyright © 2017 yuryks.info. All rights reserved.
//

import UIKit

/// Error managing structure.
struct ISErrorManager {
    
    /// Instatermii error types enumeration.
    ///
    /// - noInternetConnection: appears if no internet connection
    /// - oauthError: appears if error occures during authentication
    /// - errorParsingResponse: appears if error occures during response parsing
    /// - errorLoadingAvatar: appears if error occures during avatar loading
    /// - errorConvertingResponse: appears if error occures during response converting to JSONDictionary
    /// - unknown: any other error
    enum ISError: Swift.Error {
        case noInternetConnection
        case oauthError
        case errorParsingResponse
        case errorConvertingResponse
        case errorLoadingAvatar
        case unknown
    }
}

//MARK: - Pucbil static methods
extension ISErrorManager {
    
    /// Shows alert on top of current rootVC with error description in alert message. (Informative alert).
    ///
    /// - Parameter error: ISError
    public static func showAlert(with error: Error) {
        
        let alertTitle = "Произошла ошибка"
        let alertMessage = "\(error.localizedDescription)"
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else {
            debugPrint("appDel failed")
            return
        }
        
        appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
}
//MARK: - Private static methods
fileprivate extension ISErrorManager {

}
